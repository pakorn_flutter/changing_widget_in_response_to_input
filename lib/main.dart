import 'package:flutter/material.dart';

class Counter extends StatefulWidget {
  @override
  _CouterState createState() => _CouterState();
}

class _CouterState extends State<StatefulWidget> {
  int _counter = 0;
  void _increment() {
    setState(() {
      _counter++;
    });
    print('counter: $_counter');
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        ElevatedButton(onPressed: _increment, child: Text('Increment')),
        SizedBox(
          width: 16,
        ),
        Text('Counter: $_counter'),
      ],
    );
  }
}

void main() {
  runApp(MaterialApp(
      home: Scaffold(
    body: Center(
      child: Counter(),
    ),
  )));
}
